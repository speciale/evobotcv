Dataset
Total annotations:120
Detected annotations:90
Overall avg dev:8.08711111111
Standard dev:1.34568548781
Images:

 Filename: 001.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 002.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 003.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 004.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 005.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 006.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 007.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 008.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 009.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 010.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 011.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 012.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 013.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 014.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 015.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 016.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 017.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 018.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 019.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 020.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 021.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 022.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 023.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 024.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 025.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 026.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 027.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 028.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 029.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 030.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 031.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 8.94
	Deviation point 4: 8.49
	Avg Deviation: 8.49

-----------------------------------------------------

 Filename: 032.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 7.21
	Deviation point 4: 7.81
	Avg Deviation: 7.89

-----------------------------------------------------

 Filename: 033.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 6.4
	Deviation point 4: 8.06
	Avg Deviation: 7.75

-----------------------------------------------------

 Filename: 034.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 6.4
	Deviation point 4: 8.49
	Avg Deviation: 7.86

-----------------------------------------------------

 Filename: 035.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 6.4
	Deviation point 4: 7.81
	Avg Deviation: 7.69

-----------------------------------------------------

 Filename: 036.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 7.21
	Deviation point 4: 8.49
	Avg Deviation: 8.06

-----------------------------------------------------

 Filename: 037.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 7.21
	Deviation point 4: 8.06
	Avg Deviation: 7.96

-----------------------------------------------------

 Filename: 038.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 7.21
	Deviation point 3: 8.06
	Deviation point 4: 8.94
	Avg Deviation: 8.01

-----------------------------------------------------

 Filename: 039.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 6.4
	Deviation point 4: 8.49
	Avg Deviation: 7.86

-----------------------------------------------------

 Filename: 040.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.49
	Deviation point 2: 8.06
	Deviation point 3: 7.21
	Deviation point 4: 8.49
	Avg Deviation: 8.06

-----------------------------------------------------

 Filename: 041.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.32
	Deviation point 2: 5.66
	Deviation point 3: 8.6
	Deviation point 4: 5.66
	Avg Deviation: 6.56

-----------------------------------------------------

 Filename: 042.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.32
	Deviation point 2: 5.83
	Deviation point 3: 8.49
	Deviation point 4: 8.06
	Avg Deviation: 7.18

-----------------------------------------------------

 Filename: 043.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 9.06
	Avg Deviation: 7.26

-----------------------------------------------------

 Filename: 044.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 8.06
	Avg Deviation: 7.01

-----------------------------------------------------

 Filename: 045.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 5.66
	Avg Deviation: 6.41

-----------------------------------------------------

 Filename: 046.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.6
	Deviation point 4: 5.66
	Avg Deviation: 6.44

-----------------------------------------------------

 Filename: 047.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.32
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 5.83
	Avg Deviation: 6.57

-----------------------------------------------------

 Filename: 048.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 5.83
	Avg Deviation: 6.45

-----------------------------------------------------

 Filename: 049.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.06
	Deviation point 4: 5.83
	Avg Deviation: 6.35

-----------------------------------------------------

 Filename: 050.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 5.83
	Deviation point 2: 5.66
	Deviation point 3: 8.49
	Deviation point 4: 9.06
	Avg Deviation: 7.26

-----------------------------------------------------

 Filename: 051.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 7.62
	Deviation point 3: 5.83
	Deviation point 4: 9.22
	Avg Deviation: 7.27

-----------------------------------------------------

 Filename: 052.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 9.49
	Deviation point 3: 5.83
	Deviation point 4: 12.65
	Avg Deviation: 8.59

-----------------------------------------------------

 Filename: 053.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 5.83
	Deviation point 3: 5.83
	Deviation point 4: 12.65
	Avg Deviation: 7.68

-----------------------------------------------------

 Filename: 054.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 9.49
	Deviation point 3: 5.83
	Deviation point 4: 11.18
	Avg Deviation: 8.23

-----------------------------------------------------

 Filename: 055.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 9.49
	Deviation point 3: 5.83
	Deviation point 4: 12.08
	Avg Deviation: 8.45

-----------------------------------------------------

 Filename: 056.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 5.66
	Deviation point 2: 8.54
	Deviation point 3: 5.83
	Deviation point 4: 12.08
	Avg Deviation: 8.03

-----------------------------------------------------

 Filename: 057.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 5.83
	Deviation point 3: 5.83
	Deviation point 4: 10.3
	Avg Deviation: 7.09

-----------------------------------------------------

 Filename: 058.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 8.54
	Deviation point 3: 5.83
	Deviation point 4: 12.08
	Avg Deviation: 8.22

-----------------------------------------------------

 Filename: 059.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 9.49
	Deviation point 3: 5.83
	Deviation point 4: 12.65
	Avg Deviation: 8.59

-----------------------------------------------------

 Filename: 060.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 6.4
	Deviation point 2: 10.2
	Deviation point 3: 5.83
	Deviation point 4: 12.21
	Avg Deviation: 8.66

-----------------------------------------------------

 Filename: 061.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 11.18
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 9.48

-----------------------------------------------------

 Filename: 062.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 7.62
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 8.59

-----------------------------------------------------

 Filename: 063.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 10.05
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 9.2

-----------------------------------------------------

 Filename: 064.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 9.0
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 8.93

-----------------------------------------------------

 Filename: 065.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 8.25
	Deviation point 3: 9.49
	Deviation point 4: 10.3
	Avg Deviation: 8.96

-----------------------------------------------------

 Filename: 066.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 8.25
	Deviation point 3: 9.43
	Deviation point 4: 9.43
	Avg Deviation: 8.73

-----------------------------------------------------

 Filename: 067.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 10.05
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 9.2

-----------------------------------------------------

 Filename: 068.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 10.05
	Deviation point 3: 9.49
	Deviation point 4: 9.43
	Avg Deviation: 9.2

-----------------------------------------------------

 Filename: 069.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 8.25
	Deviation point 3: 9.49
	Deviation point 4: 10.3
	Avg Deviation: 8.96

-----------------------------------------------------

 Filename: 070.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.81
	Deviation point 2: 10.2
	Deviation point 3: 9.43
	Deviation point 4: 9.43
	Avg Deviation: 9.22

-----------------------------------------------------

 Filename: 071.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.71
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.37

-----------------------------------------------------

 Filename: 072.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.4
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.29

-----------------------------------------------------

 Filename: 073.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.71
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.37

-----------------------------------------------------

 Filename: 074.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.71
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.37

-----------------------------------------------------

 Filename: 075.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.4
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.29

-----------------------------------------------------

 Filename: 076.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.28
	Deviation point 2: 5.0
	Deviation point 3: 6.71
	Deviation point 4: 4.24
	Avg Deviation: 5.81

-----------------------------------------------------

 Filename: 077.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.4
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.29

-----------------------------------------------------

 Filename: 078.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.4
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.29

-----------------------------------------------------

 Filename: 079.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.71
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.37

-----------------------------------------------------

 Filename: 080.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.0
	Deviation point 2: 6.71
	Deviation point 3: 4.47
	Deviation point 4: 7.28
	Avg Deviation: 6.37

-----------------------------------------------------

 Filename: 081.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 7.62
	Deviation point 3: 7.07
	Deviation point 4: 14.32
	Avg Deviation: 8.93

-----------------------------------------------------

 Filename: 082.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.25
	Deviation point 2: 9.22
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 10.2

-----------------------------------------------------

 Filename: 083.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 9.22
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.81

-----------------------------------------------------

 Filename: 084.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 7.62
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.41

-----------------------------------------------------

 Filename: 085.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 8.25
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.57

-----------------------------------------------------

 Filename: 086.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 8.25
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.57

-----------------------------------------------------

 Filename: 087.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 7.62
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.41

-----------------------------------------------------

 Filename: 088.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.25
	Deviation point 2: 9.22
	Deviation point 3: 7.07
	Deviation point 4: 14.32
	Avg Deviation: 9.71

-----------------------------------------------------

 Filename: 089.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 6.71
	Deviation point 2: 9.22
	Deviation point 3: 7.07
	Deviation point 4: 14.32
	Avg Deviation: 9.33

-----------------------------------------------------

 Filename: 090.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.28
	Deviation point 2: 7.62
	Deviation point 3: 9.0
	Deviation point 4: 14.32
	Avg Deviation: 9.55

-----------------------------------------------------

 Filename: 091.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.06
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.35

-----------------------------------------------------

 Filename: 092.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.21
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.14

-----------------------------------------------------

 Filename: 093.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.06
	Deviation point 2: 7.21
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.2

-----------------------------------------------------

 Filename: 094.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.06
	Deviation point 2: 7.21
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.2

-----------------------------------------------------

 Filename: 095.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.06
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.35

-----------------------------------------------------

 Filename: 096.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.21
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.14

-----------------------------------------------------

 Filename: 097.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.25
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.4

-----------------------------------------------------

 Filename: 098.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 8.06
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.35

-----------------------------------------------------

 Filename: 099.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.21
	Deviation point 2: 7.81
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.14

-----------------------------------------------------

 Filename: 100.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 7.21
	Deviation point 2: 8.06
	Deviation point 3: 5.0
	Deviation point 4: 8.54
	Avg Deviation: 7.2

-----------------------------------------------------

 Filename: 101.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.49
	Deviation point 4: 6.71
	Avg Deviation: 7.27

-----------------------------------------------------

 Filename: 102.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.06
	Deviation point 4: 7.07
	Avg Deviation: 7.26

-----------------------------------------------------

 Filename: 103.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 5.39
	Deviation point 3: 8.6
	Deviation point 4: 6.71
	Avg Deviation: 7.53

-----------------------------------------------------

 Filename: 104.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.49
	Deviation point 4: 6.71
	Avg Deviation: 7.27

-----------------------------------------------------

 Filename: 105.jpg
 Lightness: 25
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.6
	Deviation point 4: 6.4
	Avg Deviation: 7.23

-----------------------------------------------------

 Filename: 106.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.6
	Deviation point 4: 6.71
	Avg Deviation: 7.3

-----------------------------------------------------

 Filename: 107.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.6
	Deviation point 4: 7.07
	Avg Deviation: 7.39

-----------------------------------------------------

 Filename: 108.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.06
	Deviation point 4: 6.4
	Avg Deviation: 7.09

-----------------------------------------------------

 Filename: 109.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.47
	Deviation point 3: 8.6
	Deviation point 4: 7.07
	Avg Deviation: 7.39

-----------------------------------------------------

 Filename: 110.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 9.43
	Deviation point 2: 4.24
	Deviation point 3: 8.6
	Deviation point 4: 7.07
	Avg Deviation: 7.34

-----------------------------------------------------

 Filename: 111.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 7.21
	Deviation point 3: 8.06
	Deviation point 4: 14.04
	Avg Deviation: 10.66

-----------------------------------------------------

 Filename: 112.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 8.54
	Deviation point 3: 8.6
	Deviation point 4: 14.04
	Avg Deviation: 11.13

-----------------------------------------------------

 Filename: 113.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 6.4
	Deviation point 3: 7.62
	Deviation point 4: 14.04
	Avg Deviation: 10.35

-----------------------------------------------------

 Filename: 114.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 12.37
	Deviation point 2: 8.54
	Deviation point 3: 8.6
	Deviation point 4: 14.04
	Avg Deviation: 10.89

-----------------------------------------------------

 Filename: 115.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 12.37
	Deviation point 2: 6.4
	Deviation point 3: 9.43
	Deviation point 4: 14.04
	Avg Deviation: 10.56

-----------------------------------------------------

 Filename: 116.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 12.37
	Deviation point 2: 6.4
	Deviation point 3: 7.81
	Deviation point 4: 14.04
	Avg Deviation: 10.15

-----------------------------------------------------

 Filename: 117.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 12.37
	Deviation point 2: 8.54
	Deviation point 3: 8.06
	Deviation point 4: 14.04
	Avg Deviation: 10.75

-----------------------------------------------------

 Filename: 118.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 7.21
	Deviation point 3: 7.21
	Deviation point 4: 14.04
	Avg Deviation: 10.45

-----------------------------------------------------

 Filename: 119.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 6.4
	Deviation point 3: 7.81
	Deviation point 4: 14.04
	Avg Deviation: 10.4

-----------------------------------------------------

 Filename: 120.jpg
 Lightness: 24
   Rectangle
	Deviation point 1: 13.34
	Deviation point 2: 6.4
	Deviation point 3: 7.21
	Deviation point 4: 14.04
	Avg Deviation: 10.25

-----------------------------------------------------

 Filename: 121.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 122.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 123.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 124.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 125.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 126.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 127.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 128.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 129.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 130.jpg
 Lightness: 23

-----------------------------------------------------

 Filename: 131.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 132.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 133.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 134.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 135.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 136.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 137.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 138.jpg
 Lightness: 21

-----------------------------------------------------

 Filename: 139.jpg
 Lightness: 22

-----------------------------------------------------

 Filename: 140.jpg
 Lightness: 21

-----------------------------------------------------


Errors during calculation:
[]
