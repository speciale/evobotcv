import argparse
import cv2
import numpy as np
import math
import glob
import image as im
import imgutils as imu
import json_parser as jp
img_name = None

touched = None

rectmode = True # if True, draw rectangle. Press 'm' to toggle to circle
rotmode = False # if True, AND rectmode is also True use left mouse to rotate rectangle
left_pt = []
right_pt = []

n = -1
shape_type = []
shape_center = []
shape_dimensions = []
shape_rotation = []
shape_array = [] #saves shapes in the current image

img_list = []
avg_lightness = 0
glob_box = 0

help_message = '''This function returns the corners of the selected area as: [(UpLeftcorner),(DownRightCorner)]
Use the RightButton and LeftButton of mouse and
Click on the image to set the area
Keys:
  m - Change drawing modes between rectangles and circles
  r - If drawing a rectangle toggle r to rotate
  n - New shape in same image
  d - Delete last shape
  s - Save annotated shapes to data.txt
  ESC  - Cancel annotation for this image - go to next image
'''
def new_shape():
    global img, n, left_pt, right_pt, shape_rotation, shape_center, shape_dimensions, shape_array, touched
    n += 1
    img = touched.copy()
    shape_array.append(img.copy())
    left_pt.append((-1,-1))
    right_pt.append((-1,-1))
    if (rectmode): shape_type.append('Rectangle')
    else: shape_type.append('Circle')
    shape_center.append((-1,-1))
    shape_dimensions.append(0)
    shape_rotation.append(0)

def delete_shape():
    global img, n, left_pt, right_pt, shape_rotation, shape_center, shape_dimensions, shape_array
    if len(shape_array)>1:
        del left_pt[-1], right_pt[-1], shape_rotation[-1], shape_center[-1], shape_dimensions[-1], shape_array[-1]
        n -= 1
        img = shape_array[n].copy()

def update(dummy=None):
    global shape_rotation, shape_center, shape_dimensions, touched, glob_box
    touched = img.copy()
    shape_center[n] = ((left_pt[n][0]+right_pt[n][0])/2,(left_pt[n][1]+right_pt[n][1])/2)
    if rectmode == True:
        width = right_pt[n][0]-left_pt[n][0]
        height = right_pt[n][1]-left_pt[n][1]
        shape_dimensions[n] = (width, height)
        rect = (shape_center[n], (width, height), float(shape_rotation[n]))
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        glob_box = np.array(box).tolist()
        cv2.drawContours(touched,[box], 0, (0,255,0),1)
    else:
        radius = tuple(np.subtract((shape_center[n]),left_pt[n]))
        shape_dimensions[n] = int(math.hypot(float(radius[0]), float(radius[1])))
        cv2.circle(touched,shape_center[n],shape_dimensions[n],(0,0,255),1)
    cv2.imshow('SELECT AN AREA', touched)

# mouse callback function
def on_mouse(event,x,y,flags,param):
    # global shape_rotation - don't quite
    if flags & cv2.EVENT_FLAG_LBUTTON:
        if rotmode & rectmode:
            shape_rotation[n] = left_pt[n][1] - y
        else: left_pt[n] = x,y
    if flags & cv2.EVENT_FLAG_RBUTTON:
        right_pt[n] = x,y
    update()

def select_area(image):
    global img, touched, n, shape_type, shape_center, shape_dimensions, shape_rotation, shape_array
    img = image.copy()
    touched = img.copy()
    n = -1
    shape_type = []
    shape_center = []
    shape_dimensions = []
    shape_rotation = []
    shape_array = []
    new_shape()
    update()
    cv2.namedWindow('SELECT AN AREA', cv2.WINDOW_AUTOSIZE)
    cv2.setMouseCallback('SELECT AN AREA', on_mouse)
    key_listener()

def key_listener():
    global rectmode, rotmode
    while(1):
        k = cv2.waitKey(1) & 0xFF
        if k == ord('m'):
            rectmode = not rectmode
            if (rectmode): shape_type[n] = 'Rectangle'
            else: shape_type[n] = 'Circle'
        elif k == ord('r'):
            rotmode = not rotmode
            print "Rotation: ", rotmode
        elif k == ord('n'):
            new_shape()
        elif k == ord('s'):
            save_json()
            break
        elif k == ord('d'):
            delete_shape()
        elif k == 27:
            print('Next image, nothing saved')
            break

    cv2.destroyAllWindows()

def save_json():
    annotations = []
    print "n length: ", n
    for i in range(n+1):
        if shape_type[i] == 'Rectangle':
            annotations.append(im.Rectangle(glob_box))
        elif shape_type[i] == 'Circle':
            annotations.append(im.Circle(shape_center[i], shape_dimensions[i]))
    data = im.Image(img_name, 0, annotations)
    data = data.as_dict()
    img_list.append(data)
    #jp.save_json(data, 'annotation_data.json', indent=4)
    print('Annotations where saved to log')

if __name__ == '__main__':
    global img_name, img_list, avg_lightness
    print(help_message)
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--folder", required = False, help = "Path to the folder containing images")
    # ap.add_argument("-i", "--image", required = False, help = "Path to the image")#virker ikke endnu
    args = vars(ap.parse_args())
    images = glob.glob(args["folder"]+"/*.jpg")
    for i in images:
        img_name = i
        image = cv2.imread(i)
        select_area(image)
    jp.save_json(img_list, "annotation_data.json", indent=4)

