#imutils.py

from matplotlib import pyplot as plt
import numpy as np
import cv2

def image_blur(image, window=(5,5), ksize=0, blur=None, bil_param1=21, bil_param2=21):
    if blur is None:
        newImage = image
    elif blur is "gaussian":
        newImg = cv2.GaussianBlur(image, window, ksize)
    elif blur is "avg":
        newImg = cv2.blur(newImg, window)
    elif blur is "median":
        newImg = cv2.medianBlur(image, window[0])
    elif blur is "bilateral":
        newImg = cv2.bilateralFilter(image, window[0], bil_param1, bil_param2)
    return newImg

def image_subtract(background, image, value=0):
    return cv2.subtract(image, background + value)

def convert_image_color(image, _type=None):
    (B,G,R) = cv2.split(image)
    if _type is None:
        return image;
    elif _type is "rgb":
        newImage = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    elif _type is "red":
        newImage = R
    elif _type is "blue":
        newImage = B
    elif _type is "green":
        newImage = G
    return newImage


def translate(image, x, y):
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
    return shifted

def rotate(image, angle, center = None, scale = 1.0):
    (h, w) = image.shape[:2]
    if center is None:
        center = (w / 2, h / 2)
    M = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated

def resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]
    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(image, dim, interpolation = inter)
    return resized

def plot_grayscale_histogram(image, title, mask = None):
    img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    plt.figure()
    plt.title(title)
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    hist = cv2.calcHist([img], [0], mask, [256], [0, 256])
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.show()

def plot_color_histogram(image, title, mask = None):
    chans = cv2.split(image)
    colors = ("b", "g", "r")
    plt.figure()
    plt.title(title)
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    for (chan, color) in zip(chans, colors):
        hist = cv2.calcHist([chan], [0], mask, [256], [0,256])
        plt.plot(hist, color = color)
        plt.xlim([0,256])
    plt.show()

def avg_lightness(rgb_image):
    hls = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2HLS)
    h,l,s = cv2.split(hls)
    total = np.sum(l)
    avg = total / l.size
    return avg

def avg_luma(rgb_image):
    yuv = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2YUV)
    y,u,v = cv2.split(yuv)
    total = np.sum(y)
    avg = total / y.size
    return avg
