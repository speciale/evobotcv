#json_parser.py
import json
import image
import cv2
import numpy as np
def parse_json(data):
    img_list = []
    for img in data:
        img_obj = image.Image("", 0, [])
        img_obj.avg_lightness = img["image"]["avg_lightness"]
        img_obj.filename = img["image"]["filename"]
        img_obj.annotations = img["image"]["annotations"]
        if(len(img_obj.annotations) > 0):
            for x in range(0, len(img_obj.annotations)):
                if (img_obj.annotations[x]["shape"] == "Circle"):
                    img_obj.annotations[x] = image.Circle(img_obj.annotations[x]["center"], img_obj.annotations[x]["radius"])
                else:
                    img_obj.annotations[x] = image.Rectangle(img_obj.annotations[x]["box"])
        img_list.append(img_obj)
    return img_list

def add_from_file(data):
    with open(data) as data_file:
        data = json.load(data_file)
    return data

def save_json(data, filename, indent=None):
    with open(filename, 'a') as outfile:
        json.dump(data, outfile, indent=indent)

