import cv2
import os
import argparse
import numpy as np
import square_detect as sqr_detect
import crcl_detect
import glob
import image as im
import imgutils as imu
import json_parser as jp

img_objects = []
num_of_objs = 0

def process_pics_visual_spectrum(folder, new_folder):
    global img_objects, num_of_objs
    bgs = glob.glob(folder+'bg/*.jpg')
    for i, path in enumerate(imgs):
         #grab the filename from the path
        img_name = os.path.basename(path)
        img = cv2.imread(path)
        bg = cv2.imread(bgs[i%5], 0)
        avg_lightness = int(imu.avg_lightness(img.copy()))
        img, rectangles = sqr_detect.find_squares(img, bg)
        img, circles = crcl_detect.find_circles(img)
        img_objects.append(im.Image(img_name, avg_lightness, rectangles + circles).as_dict())
        cv2.imwrite(os.path.join(new_folder, img_name), img)

def process_pics_ir(folder, new_folder):
    global img_objects, num_of_objs
    bg = cv2.imread(folder+"bg/bg2.jpg", 0)
    for path in imgs:
       #grab the filename from the path
        img_name = os.path.basename(path)
        print img_name
        img = cv2.imread(path)
        avg_lightness = int(imu.avg_lightness(img.copy()))
        img, rectangles = sqr_detect.find_squares(img, bg)
        if len(rectangles) > 0:
            num_of_objs += 1
            print img_name
        img, circles = crcl_detect.find_circles(img)
        img_objects.append(im.Image(img_name, avg_lightness, rectangles + circles).as_dict())
        cv2.imwrite(os.path.join(new_folder, img_name), img)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--folder", required = True, help = "Path to the folder containing images")
    ap.add_argument("-t", "--type", required = True, help = "Type of images: VIS for visual spectrum images or IR for infrared images")
    args = vars(ap.parse_args())
    folder = args["folder"]
    typeOfImgs = args["type"]
    new_folder = folder+"/processedImages"
    if not os.path.exists(new_folder):
        os.mkdir(new_folder)
    imgs = glob.glob(folder+"*.jpg")
    if typeOfImgs == "VIS":
        process_pics_visual_spectrum(folder, new_folder)
    elif typeOfImgs == "IR":
        process_pics_ir(folder, new_folder)

    jp.save_json(img_objects, "data.json", indent=4)
    print num_of_objs
