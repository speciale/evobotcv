from enum import Enum
from math import hypot
import image
import math
import numpy as np
import json_parser
import cv2
import matplotlib.pyplot as plt
import argparse

class Prop(Enum):
    center = "center"
    radius = "Radius"
    circle = "Circle"
    rectangle = "Rectangle"
    width = "width"
    height = "height"
    rotation = "rotation"
    box = "box"

class Report(object):
    def __init__(self, annotations, detected, annotations_dev_list,
                img_obj_dev_list, img_drawed_list, error_list):
        #Antal annotations (ground truth)
        self.total_annotations = annotations
        #Antal fundne annotations
        self.detected = detected
        #Liste med annotations
        self.annotations_dev_list = annotations_dev_list
        #liste med hvert billede (img_obj_data)
        self.img_obj_dev_list = img_obj_dev_list
        #liste med billeder, der viser begge annoteringer.
        self.img_drawed_list = img_drawed_list
        #liste med eventuelle fejlbeskeder
        self.error_list = error_list
        #den gennemsnitlige afvigelse for et helt datasettet
        self.avg_dev = avg_dev(annotations_dev_list)
        #Standard afvigelsen for et helt datasettet
        self.stdDev = standardDev(annotations_dev_list)

    def __str__(self):
        dev_string = ""
        for img_dev_obj in self.img_obj_dev_list:
            dev_string = dev_string + str(img_dev_obj)
        return "Dataset\nTotal annotations:{0}\nDetected annotations:{1}\nOverall avg dev:{4}\nStandard Dev:{5}\nImages:\n{2}\n\nErrors during calculation:\n{3}".format(self.total_annotations, self.detected, dev_string, error_list, self.avg_dev, self.stdDev)

class circle_data(object):
    def __init__(self, center, radius):
        self.shape = Prop.circle
        self.center = center
        self.radius = radius
        self.avg_deviation = round((center+radius)/2, 2)

    def __str__(self):
        return "   Circle\n\tDeviation center: {0}\n\tDevation radius: {1}\n\tAvg Deviation: {2}".format(self.center, self.radius, self.avg_deviation)

class rectangle_data(object):
    def __init__(self,box):
        self.shape = Prop.rectangle
        self.box = box
        self.p1 = round(box[0],2)
        self.p2 = round(box[1],2)
        self.p3 = round(box[2],2)
        self.p4 = round(box[3],2)
        self.avg_deviation = round((box[0] + box[1] + box[2] + box[3])/4, 2)

    def __str__(self):
        return "   Rectangle\n\tDeviation point 1: {0}\n\tDeviation point 2: {1}\n\tDeviation point 3: {2}\n\tDeviation point 4: {3}\n\tAvg Deviation: {4}".format(self.p1, self.p2, self.p3, self.p4, self.avg_deviation)

class image_obj_data(object):
    def __init__(self, annotation_dev_list, filename, avg_lightness):
        self.filename = filename
        self.avg_lightness = avg_lightness
        self.annotation_dev_list = annotation_dev_list

    def __str__(self):
        annotation_string = ""
        for ann in self.annotation_dev_list:
            annotation_string = annotation_string + (str(ann) + "\n")
        return "\n Filename: {0}\n Lightness: {1}\n{2}\n-----------------------------------------------------\n".format(self.filename, self.avg_lightness, annotation_string)

def standardDev(annotations_dev_list):
    avg_dev = []
    for ann in annotations_dev_list:
        avg_dev.append(ann.avg_deviation)
    return np.std(avg_dev)

def avg_dev(annotations_dev_list):
    if len(annotations_dev_list) is []:
        return 0
    else:
        total = 0
        for ann in annotations_dev_list:
            if ann.avg_deviation is not None:
                total = total + ann.avg_deviation
        return float(total)/len(annotations_dev_list)

def circle_list(annotations):
    circles = []
    for ann in annotations:
        if ann.shape == Prop.circle:
            circles.append(ann)
    return circles

def rect_list(annotations):
    rects = []
    for ann in annotations:
        if ann.shape == Prop.rectangle:
            rects.append(ann)
    return rects

##############################################################################

#Evaluation and count methods

def eval_ann_count(img_obj, truth_img_obj):
    return count_ann_obj(img_obj) is count_ann_obj(truth_img_obj)

def eval_object_count(img_obj_list, truth_img_list):
    return count_objects(img_obj_list) is count_objects(truth_img_list)

def count_objects(img_obj_list):
    return len(img_obj_list)

def count_ann_obj(img_obj):
    return len(img_obj.annotations)

def compare_shape(img_obj, truth_img_obj, x):
    return img_obj.annotations[x].shape is truth_img_obj.annotations[x].shape

def count_ann_dataset(dataset):
    count = 0
    for image in dataset:
        for annotations in image.annotations:
            count = count + 1
    return count

##############################################################
def draw_ann(image, annotations, truth_annotations):
    for annotation in annotations:
        if annotation.shape is Prop.circle:
            image = cv2.circle(image, (annotation.center[0], annotation.center[1]), annotation.radius,(0,0,255),2)
        elif annotation.shape is Prop.rectangle:
            box = np.int0(annotation.box)
            image = cv2.drawContours(image,[box], 0, (0,0,255),2)
    for truth in truth_annotations:
        if truth.shape is Prop.circle:
            image = cv2.circle(image, (truth.center[0], truth.center[1]), truth.radius, (0,255,0), 2)
        elif truth.shape is Prop.rectangle:
            box = np.int0(truth.box)
            image = cv2.drawContours(image,[box], 0, (0,255,0),2)
    return image

def calc_dev_ann(img_ann, truth_ann, prop):
    if prop == Prop.center:
        x1 = img_ann.center[0]
        y1 = img_ann.center[1]
        x2 = truth_ann.center[0]
        y2 = truth_ann.center[1]
        xdif = (x1-x2)**2
        ydif = (y1-y2)**2
        return round(math.sqrt(xdif+ydif),2)
    elif prop == Prop.radius:
        return round(abs(img_ann.radius-truth_ann.radius),2)
    elif prop == Prop.box:
        p1_i, p2_i, p3_i, p4_i = img_ann.box
        p1_t, p2_t, p3_t, p4_t = truth_ann.box
        for point in img_ann.box:
            dist_1 = math.hypot(p1_i[0] - p1_t[0], p1_i[1] - p1_t[1])
            dist_2 = math.hypot(p1_i[0] - p2_t[0], p1_i[1] - p2_t[1])
            dist_3 = math.hypot(p1_i[0] - p3_t[0], p1_i[1] - p3_t[1])
            dist_4 = math.hypot(p1_i[0] - p4_t[0], p1_i[1] - p4_t[1])
            p1_c = min(dist_1, dist_2, dist_3, dist_4)
            dist_1 = math.hypot(p2_i[0] - p1_t[0], p2_i[1] - p1_t[1])
            dist_2 = math.hypot(p2_i[0] - p2_t[0], p2_i[1] - p2_t[1])
            dist_3 = math.hypot(p2_i[0] - p3_t[0], p2_i[1] - p3_t[1])
            dist_4 = math.hypot(p2_i[0] - p4_t[0], p2_i[1] - p4_t[1])
            p2_c = min(dist_1, dist_2, dist_3, dist_4)
            dist_1 = math.hypot(p3_i[0] - p1_t[0], p3_i[1] - p1_t[1])
            dist_2 = math.hypot(p3_i[0] - p2_t[0], p3_i[1] - p2_t[1])
            dist_3 = math.hypot(p3_i[0] - p3_t[0], p3_i[1] - p3_t[1])
            dist_4 = math.hypot(p3_i[0] - p4_t[0], p3_i[1] - p4_t[1])
            p3_c = min(dist_1, dist_2, dist_3, dist_4)
            dist_1 = math.hypot(p4_i[0] - p1_t[0], p4_i[1] - p1_t[1])
            dist_2 = math.hypot(p4_i[0] - p2_t[0], p4_i[1] - p2_t[1])
            dist_3 = math.hypot(p4_i[0] - p3_t[0], p4_i[1] - p3_t[1])
            dist_4 = math.hypot(p4_i[0] - p4_t[0], p4_i[1] - p4_t[1])
            p4_c = min(dist_1, dist_2, dist_3, dist_4)
        return [p1_c, p2_c, p3_c, p4_c]


def annotations_object_dev(img_ann, truth_ann, shape):
    if shape == Prop.circle:
        center = calc_dev_ann(img_ann, truth_ann, Prop.center)
        radius = calc_dev_ann(img_ann, truth_ann, Prop.radius)
        return circle_data(center, radius)
    elif shape == Prop.rectangle:
        box = calc_dev_ann(img_ann, truth_ann, Prop.box)
        return rectangle_data(box)

def image_object_dev(img_obj, truth_img_obj):
    annotations_list = []
    if img_obj.filename == truth_img_obj.filename: #same amount of annotations in objects?
        for x in range(0, len(img_obj.annotations)):
            if compare_shape(img_obj, truth_img_obj, x) and img_obj.annotations[x].shape == Prop.circle:
                annotations_list.append(annotations_object_dev(img_obj.annotations[x], truth_img_obj.annotations[x], Prop.circle))
            elif compare_shape(img_obj, truth_img_obj, x) and img_obj.annotations[x].shape == Prop.rectangle:
                annotations_list.append(annotations_object_dev(img_obj.annotations[x], truth_img_obj.annotations[x], Prop.rectangle))
            else:
                error_list.append("Cannot compare objects that are not same shape in file " + img_obj.filename)
    else:
        error_list.append("not same filename")
    return image_obj_data(annotations_list, img_obj.filename, img_obj.avg_lightness)

def generate_report(dataset, truthset):
    img_drawed_list = []
    img_list = []
    annotations_list = []
    annotations_count_truth = count_ann_dataset(truthset)
    annotations_count_auto = count_ann_dataset(dataset)

    if eval_object_count(dataset, truthset): #same amount of pictures in both sets?
        for x in range(0, len(dataset)):
            if path is not None:
                if len(dataset[x].annotations) > 0:
                    drawed = draw_ann(cv2.imread(path + "/final_set/" + dataset[x].filename), dataset[x].annotations, truthset[x].annotations)
                    img_drawed_list.append(drawed)
                    cv2.imwrite(path + "/report_img/" + dataset[x].filename, drawed)
            image = image_object_dev(dataset[x], truthset[x])
            img_list.append(image)
            for annotation in image.annotation_dev_list:
                annotations_list.append(annotation)
    else:
        error_list.append("The truth set does not correspond with the test set.")

    return Report(annotations_count_truth, annotations_count_auto, annotations_list, img_list, img_drawed_list, error_list)


##############################################################

error_list = []
path = None

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--detected", required = True, help = "Path to the json file containing detection data")
    ap.add_argument("-a", "--annotation", required = True, help = "Path to the json file containing annotation data")
    ap.add_argument("-p", "--path", required = False, help = "Set this to the root datasetfolder, if you want report images to be generated")
    args = vars(ap.parse_args())
    d = args["detected"]
    a = args["annotation"]
    path = args["path"]

    detected = json_parser.parse_json(json_parser.add_from_file(d))
    truth = json_parser.parse_json(json_parser.add_from_file(a))
    report = generate_report(detected, truth)

    print report
