import cv2
import numpy as np
import image as im

def angle_cos(p0, p1, p2):
    d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
    return abs(np.dot(d1,d2) / np.sqrt(np.dot(d1,d1)*np.dot(d2, d2)))

def find_squares(img, bg):
    cimg = img
    rectangles = []
    img = cv2.cvtColor(img.copy(),cv2.COLOR_BGR2GRAY)
    img, bg = img*2, bg*2
    subtracted = cv2.subtract(img, bg)
    enhanced_contrast = subtracted*3
    eroded = cv2.erode(enhanced_contrast, np.ones((2, 2)))
    blurred = cv2.bilateralFilter(eroded,5,21,21)
    dilated = cv2.dilate(blurred, np.ones((9, 9)))
    canny = cv2.Canny(dilated, 16, 39)
    dilated = cv2.dilate(canny, np.ones((5, 5)))
    cv2.imshow("test", dilated)

    _, contours, _ = cv2.findContours(dilated, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        cnt_len = cv2.arcLength(cnt, True)
        cnt = cv2.approxPolyDP(cnt, 0.07*cnt_len, True)
        if len(cnt) == 4 and cv2.contourArea(cnt) > 25000 and cv2.contourArea(cnt) < 50000 and cv2.isContourConvex(cnt):
            print "before", cnt.shape()
            cnt = cnt.reshape(-1,2)
            print "after", cnt.shape()
            max_cos = np.max([angle_cos(cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4]) for i in xrange(4)])
            if max_cos < 0.1:
                rectangle_object = im.Rectangle(np.array(cnt).tolist())
                rectangles.append(rectangle_object)
                cv2.drawContours(cimg, [cnt], 0, (0, 255, 0), 2)
    return cimg, rectangles
