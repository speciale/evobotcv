import cv2
import numpy as np

def LoadCalibrationFromFile(cameraID):
    path = "./CalibrationData/"
    cameraMatrix = np.load(path + "Camera_" + str(1) + "_cameraMatrix.npy")
    distCoeffs = np.load(path + "Camera_" + str(1) + "_distCoeffs.npy")
    return cameraMatrix, distCoeffs

def avg_lightness(rgb_image):
    hls = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2HLS)
    h,l,s = cv2.split(hls)
    total = np.sum(l)
    avg = total / l.size
    return avg

def draw_lines(image, text):
    h,w,c = image.shape
    cv2.line(image,(w/2,0), (w/2,h), (0,255,0))
    cv2.line(image,(0,h/2), (w,h/2), (0,255,0))
    cv2.line(image,(w,0), (w/2,h/2), (0,255,0))
    cv2.line(image,(w/2,0),(w,h/2), (0,255,0))
    cv2.putText(image, text, color=(0,255,0), org=(w-110,30), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1)
    return image

if __name__ == '__main__':
    cameraID = 1
    cap = cv2.VideoCapture(cameraID)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

    image = 0

    while(True):
        ret, frame = cap.read()

        #Intr, distC = LoadCalibrationFromFile(cameraID)
        #undistorted = cv2.undistort(frame, Intr, distC)
        #visual = undistorted.copy()

        #lightness = avg_lightness(frame)
        #lines = draw_lines(visual, str(lightness))

        cv2.imshow('undistorted',frame)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('q'):
            break;
        elif k == ord('p'):
            print "saving image", image
            cv2.imwrite("vis-{0}.jpg".format(image), frame)
            image = image + 1

    cap.release()
    cv2.destroyAllWindows()
