class Image(object):
    def __init__(self,filename,avg_lightness,annotations):
        self.filename = filename
        self.avg_lightness = avg_lightness
        self.annotations = annotations

    def as_dict(self):
        anno_dict = []
        for shape in self.annotations:
            anno_dict.append(shape.__dict__)
        return {'image':{'filename':self.filename, 'avg_lightness':self.avg_lightness, 'annotations':anno_dict}}

class Rectangle(object):
    def __init__(self,box):
        self.shape = self.__class__.__name__
        self.box = box

class Circle(object):
    def __init__(self, center, radius):
        self.shape = self.__class__.__name__
        self.center = center
        self.radius = radius
