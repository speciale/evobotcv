import cv2
import numpy as np
import image as im
import imgutils as imu

def find_circles(img):
    cimg = img
    img = cv2.cvtColor(img.copy(),cv2.COLOR_BGR2GRAY)
    img = imu.image_blur(img, blur="median", window=(5,5))
    detections = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 2, 200, minRadius=120, maxRadius=140)
    if detections is None:
        return cimg, []
    else:
        # return the detection values rounded to 0 decimals, with unsigned integer from 0 to 65535
        detections = np.uint16(np.around(detections))
        circles = []
        for circle in detections[0,:]:
            # draw the circle on the image
            cv2.circle(cimg, (circle[0], circle[1]), circle[2], (0,255,0), 2)
            # draw the center of the circle
            cv2.circle(cimg, (circle[0], circle[1]), 2, (0,0,255),3)
            x, y, r = int(circle[0]), int(circle[1]), int(circle[2])
            # make the circle_object and add it to circles
            circle_object = im.Circle((x,y), r)
            circles.append(circle_object)
        return cimg, circles
